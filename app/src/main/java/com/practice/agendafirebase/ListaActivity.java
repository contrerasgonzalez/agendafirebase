package com.practice.agendafirebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.practice.agendafirebase.Objetos.Contactos;
import com.practice.agendafirebase.Objetos.ReferenciasFirebase;

import java.util.ArrayList;

public class ListaActivity extends ListActivity {
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private Button btnNuevo;
    final Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        database = FirebaseDatabase.getInstance();
        reference = database.getReferenceFromUrl(ReferenciasFirebase.URL_DATABASE + ReferenciasFirebase.DATABASE_NAME + "/" +
                ReferenciasFirebase.TABLE_NAME);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        obtenerContactos();
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    private void obtenerContactos(){
        final ArrayList<Contactos> contactos = new ArrayList<>();

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Contactos contacto = dataSnapshot.getValue(Contactos.class);
                contactos.add(contacto);
                final MyArrayAdapter adapter = new MyArrayAdapter(context,R.layout.layout_contacto,contactos);
                setListAdapter(adapter);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        reference.addChildEventListener(listener);
    }

    class MyArrayAdapter extends ArrayAdapter<Contactos>{
        Context context;
        int textViewResourceId;
        ArrayList<Contactos> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Contactos> objects){
            super(context,textViewResourceId,objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View converView, ViewGroup parent){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, parent,false);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = (TextView) view.findViewById(R.id.lblTelefonoContacto);
            Button btnModificar = (Button) view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button) view.findViewById(R.id.btnBorrar);
            if(objects.get(position).getFavorito()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }else{
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    borrarContacto(objects.get(position).getID());
                    objects.remove(position);
                    notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),"Contacto eliminado con exito",Toast.LENGTH_SHORT).show();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto",objects.get(position));
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }
            });
            return view;
        }

        public void borrarContacto(String childIndex){
            reference.child(String.valueOf(childIndex)).removeValue();
        }
    }
}

